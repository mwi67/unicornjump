//load only when all html has been written
document.addEventListener('DOMContentLoaded',() => {
    //query selector is a method that allows to pick out element from index.html (class of grid)
    const grid = document.querySelector('.grid')
    //create element creates div
    const doodler = document.createElement('div')

    //global variables
    let doodlerLeftSpace = 50
    let startPoint = 150
    let doodlerBottomSpace = startPoint
    let isGameOver = false
    let platformCount = 5
    let platforms = []
    let upTimerId
    let downTimerId
    let isJumping = true
    let isGoingLeft = false
    let isGoingRight = false
    let leftTimerId
    let rightTimerId
    let score = 0
    let jumpAudio = new Audio('twinkle.m4a')
    let gameItem = null
    let gameItemTimerId
    let gameItemMoveTimerId

    //create doodler and put it on the grid
    function createDoodler() {
        //puts doodler in the grid
        grid.appendChild(doodler)
        //add googler class
        doodler.classList.add('doodler')
        doodlerLeftSpace = platforms[0].left
        doodler.style.left = doodlerLeftSpace + 'px'
        doodler.style.bottom = doodlerBottomSpace + 'px'
    }

    //class for platform floating
    class Platform {
        constructor(newPlatformBottom) {
            //constructor variables
            this.bottom = newPlatformBottom
            this.left = Math.random() * 315
            this.visual = document.createElement('div')

            // style the platform
            const visual = this.visual
            visual.classList.add('platform')
            visual.style.left = this.left + 'px'
            visual.style.bottom = this.bottom + 'px'
            grid.appendChild(visual)
        }
    }

    //class for item
    class Item {
        constructor(addition, itemType) {
            this.bottom = 600
            this.left = Math.random() * 315
            this.number = addition
            this.visual = document.createElement('div')
            this.type = itemType

            const visual = this.visual
            visual.classList.add(this.type)
            visual.style.left = this.left + 'px'
            visual.style.bottom = this.bottom + 'px'
            grid.appendChild(visual)
        }
    }

    //create items randomly
    function createItems() {
        if (gameItem != null) { return; }
        let appearingProb = Math.random() * 10

        if (appearingProb < 5) {
            gameItem = new Item(5, 'addItem')
        } else {
            gameItem = new Item(-5, 'minusItem')
        }
    }

    //move the item from the top of the screen
    function moveItem() {
        if (gameItem == null) return;
        gameItem.bottom -= 4
        let visual = gameItem.visual
        visual.style.bottom = gameItem.bottom + 'px'

        if (gameItem.bottom <= 0) {
            gameItem.visual.classList.remove(gameItem.type)
            gameItem = null;
        }

        if(gameItem && hit()){
                // score += gameItem.number
                // gameItem.visual.classList.remove(gameItem.type)
                // gameItem = null;
                console.log('points up')
        }

    }

    //define when unicorn can get items, unicorn 80x85 and item 40x40
    function hit() {

        let doodlerRight = doodlerLeftSpace + 80
        let doodlerTop = doodlerBottomSpace + 85
        let itemRight = gameItem.left + 40
        let itemTop = gameItem.Top + 40

        let i = (
            (doodlerLeftSpace <= itemRight) &&
                (doodlerRight <= itemRight.left) &&
                (doodlerTop >= gameItem.bottom) &&
                (doodlerBottomSpace <= itemTop) ||//if unicorn's left hits item's right

            (doodlerRight >= gameItem.left) &&
                (doodlerLeftSpace <= itemRight) &&
                (doodlerTop >= gameItem.bottom) &&
                (doodlerBottomSpace <= itemTop) || //if unicorn's right hits item's left

            (doodlerBottomSpace >= gameItem.bottom) && 
                (doodlerBottomSpace <= itemTop) && 
                (doodlerRight >= gameItem.left) && 
                (doodlerLeftSpace <= itemRight) || //if unicorn's bottom hits top of the item

            (doodlerBottomSpace <= gameItem.bottom) && 
                (doodlerBottomSpace >= itemTop) && 
                (doodlerRight >= gameItem.left) && 
                (doodlerLeftSpace <= itemRight) //if unicorn's top hits the bottom of the item

        ); // becomes true or false
        
        console.log(i)

        return i
    }


    //creates platform for the doodler to jump on
    function createPlatforms() {
        for (let i =0; i < platformCount; i++) {
            //gap between platforms
            let platGap = 600 / platformCount
            //as you loop, increase the gap of the platform from the bottom so each platform has different level
            let newPlatformBottom = 100 + i * platGap
            let newPlatform = new Platform(newPlatformBottom)

            platforms.push(newPlatform)
            console.log(platforms)
        }
    }
    
    //moves platforms down
    function movePlatforms() {
        //if doodler is above 200px from the bottom
        if(doodlerBottomSpace > 200) {
            platforms.forEach(platform => {
                platform.bottom -= 4
                let visual = platform.visual
                visual.style.bottom = platform.bottom + 'px'

                if (platform.bottom < 10) {
                    let firstplatform = platforms[0].visual
                    firstplatform.classList.remove('platform')
                    platforms.shift()
                    score ++
                    console.log(platforms)
                    let newPlatform = new Platform(600)
                    platforms.push(newPlatform)
                }
            })
        }
    } 

    //jump up while the upTimerId is set
    function jump() {
        clearInterval(downTimerId)
        isJumping = true
        const audio = jumpAudio.cloneNode()
        audio.play();
        upTimerId = setInterval(function(){
            doodlerBottomSpace += 20
            doodler.style.bottom = doodlerBottomSpace + 'px'
            if(doodlerBottomSpace > startPoint + 200) {
                fall()
            } 
        }, 30)
    }

    //fall down while downTimerId is set
    function fall() {
        clearInterval(upTimerId)
        isJumping = false
        downTimerId = setInterval(function() {
            doodlerBottomSpace -= 5
            doodler.style.bottom = doodlerBottomSpace + 'px'

            if(doodlerBottomSpace <= 0) {
                gameOver()
            }

            platforms.forEach(platform => {
                if(
                    //check if doodler landed on the platform
                    (doodlerBottomSpace >= platform.bottom) &&
                    (doodlerBottomSpace <= platform.bottom + 15) &&
                    ((doodlerLeftSpace + 60) >= platform.left) &&
                    (doodlerLeftSpace <= (platform.left + 85)) &&
                    !isJumping
                ){
                    startPoint = doodlerBottomSpace
                    jump()
                }
            })
        },30)
    }

    //game over stop the game
    function gameOver() {
        console.log('game over');
        isGameOver = true
        while (grid.firstChild) {
            grid.removeChild(grid.firstChild)
        }
        grid.innerHTML = score
        clearInterval(upTimerId)
        clearInterval(downTimerId)
        clearInterval(leftTimerId)
        clearInterval(rightTimerId)
        clearInterval(gameItemTimerId)
        clearInterval(gameItemMoveTimerId)
    }

    //allows player to move with arrow keys
    function control(e) {
        if(e.key === "ArrowLeft"){
            moveLeft()
        } else if(e.key === "ArrowRight"){
            moveRight()
        } else if(e.key === "ArrowUp") {
            moveStraight()
        }
    }

    function moveStraight() {
        isGoingLeft = false
        isGoingRight = false
        clearInterval(rightTimerId)
        clearInterval(leftTimerId)
    }
 
    function moveLeft() {
        if (isGoingLeft) {
            return
        }
        if (isGoingRight){
            clearInterval(rightTimerId)
            isGoingRight = false
        }
        isGoingLeft = true
        leftTimerId = setInterval(function (){
            if (doodlerLeftSpace >= 0){
                doodlerLeftSpace -= 5
                doodler.style.left = doodlerLeftSpace + 'px'
            } else moveRight()
        },20)
    }

    function moveRight() {
        if (isGoingRight) {
            return
        }
        if (isGoingLeft){
            clearInterval(leftTimerId)
            isGoingLeft = false
        }
        isGoingRight = true
        rightTimerId = setInterval(function(){

            if (doodlerLeftSpace <= 340) {
                doodlerLeftSpace += 5
                doodler.style.left = doodlerLeftSpace + 'px'
            } else moveLeft()
        }, 20)
    }

    function start() {
        if (!isGameOver) {
            createPlatforms()
            createDoodler()
            setInterval(movePlatforms, 30)
            gameItemTimerId = setInterval(createItems, 2000)
            gameItemMoveTimerId = setInterval(moveItem, 10)
            jump()
            document.addEventListener('keyup', control)
        }
    }
    start()
})